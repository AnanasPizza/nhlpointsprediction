#!/usr/bin/env python
# coding: utf-8

# In[42]:


import numpy as np
import pandas as pd
from pandas.io import sql

import sportsreference.nhl as NHL
from sportsreference.nhl.teams import Teams
from sportsreference.nhl.schedule import Schedule

from sklearn import datasets, linear_model
from sklearn.model_selection import cross_val_score, KFold
from keras.models import Sequential
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from sklearn.metrics import r2_score,mean_squared_error
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Ridge
from sklearn.datasets import make_regression


from tensorflow import keras

import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

from keras.optimizers import RMSprop
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor

import joblib

from sqlalchemy import create_engine
from sqlalchemy.types import Integer
import pymysql
import matplotlib as mathplt
import matplotlib.pyplot as plt


import os
import pickle
import sys, getopt
import argparse
from datetime import date

db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
db_connection = create_engine(db_connection_str)

def createViewLearning():
    db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
    db_connection = create_engine(db_connection_str)
    #Tabelle learning löschen
    db_connection.execute("Drop table if exists learning")
    #Alle Spiele die gespielt wurden werden aus der schedule tabelle geholt
    ResultProxy = db_connection.execute("Select date, home_team, away_team, home_goals, away_goals, boxscore_index from schedule where home_goals is not null order by date asc")
    ResultSetSchedule = ResultProxy.fetchall()
    for game in ResultSetSchedule:
        #Datum und TeamIDs werden ausgelesen pro Spiel 
        datum = game[0]
        home_goals = game[3]
        away_goals = game[4]
        heim_id = db_connection.execute("Select id from teams where team = '"+str(game[1])+"'").fetchall()[0][0]
        gast_id = db_connection.execute("Select id from teams where team = '"+str(game[2])+"'").fetchall()[0][0]
        #Daten des Teams am jeweiligen Spieltag werden geladen.
        ResultSetHeim = db_connection.execute("Select assists, even_strength_goals,"+
                                              "even_strength_assists,"+
                                              "penalties_in_minutes,"+
                                              "power_play_assists,"+
                                              "save_percentage,"+
                                              "saves,"+
                                              "shooting_percentage,"+
                                              "short_handed_assists,"+
                                              "shots_on_goal,"+
                                              "shutout,"+
                                              "duration,"+
                                              "corsi,"+
                                              "corsi_percentage,"+
                                              "faceoff_wins,"+
                                              "faceoff_win_percentage,"+
                                              "fenwick,"+
                                              "fenwick_percentage,"+
                                              "offensive_zone_start,"+
                                              "power_play_opportunities,"+
                                              "pdo,"+
                                              "goals"+
                                              " from boxscores where team_id = '"+str(heim_id)+"' and date = '"+str(datum)+"'").fetchall()
        ResultSetGast = db_connection.execute("Select assists, even_strength_goals,"+
                                              "even_strength_assists,"+
                                              "penalties_in_minutes,"+
                                              "power_play_assists,"+
                                              "save_percentage,"+
                                              "saves,"+
                                              "shooting_percentage,"+
                                              "short_handed_assists,"+
                                              "shots_on_goal,"+
                                              "shutout,"+
                                              "duration,"+
                                              "corsi,"+
                                              "corsi_percentage,"+
                                              "faceoff_wins,"+
                                              "faceoff_win_percentage,"+
                                              "fenwick,"+
                                              "fenwick_percentage,"+
                                              "offensive_zone_start,"+
                                              "power_play_opportunities,"+
                                              "pdo,"+
                                              "goals "+
                                              "from boxscores where team_id = '"+str(gast_id)+"' and date = '"+str(datum)+"'").fetchall()
        if(ResultSetHeim[0][0] is not None and ResultSetGast[0][0] is not None):
            #ResultSetGast = db_connection.execute("Select avg(assists) from boxscores where team_id = '"+str(gast_id)+"' and date < '"+str(datum)+"'")
            d = {'date':[datum], 'home_goals':[home_goals], 'away_goals':[away_goals],
                 'home_id':[heim_id],'away_id':[gast_id],
                'assists_heim':[ResultSetHeim[0][0]],    
                'even_strength_goals_heim' : [ResultSetHeim[0][1]],
                'even_strength_assists_heim' : [ResultSetHeim[0][2]],
                'penalties_in_minutes_heim' : [ResultSetHeim[0][3]],
                'power_play_assists_heim' : [ResultSetHeim[0][4]], 
                'save_percentage_heim' : [ResultSetHeim[0][5]], 
                'saves_heim' : [ResultSetHeim[0][6]],
                'shooting_percentage_heim' :[ResultSetHeim[0][7]],
                'short_handed_assists_heim' :[ResultSetHeim[0][8]], 
                'shots_on_goal_heim' :[ResultSetHeim[0][9]], 
                'shutout_heim' : [ResultSetHeim[0][10]],
                'duration_heim' : [ResultSetHeim[0][11]],
                'corsi_heim' : [ResultSetHeim[0][12]],
                'corsi_percentage_heim' : [ResultSetHeim[0][13]],
                'faceoff_wins_heim' : [ResultSetHeim[0][14]],
                'faceoff_win_percentage_heim' : [ResultSetHeim[0][15]],
                'fenwick_heim' : [ResultSetHeim[0][16]],
                'fenwick_percentage_heim' : [ResultSetHeim[0][17]],
                'offensive_zone_start_heim' : [ResultSetHeim[0][18]],
                'power_play_opportunities_heim' : [ResultSetHeim[0][19]],
                'pdo_heim' : [ResultSetHeim[0][20]],
                'goals_heim' : [ResultSetHeim[0][21]],
                'assists_gast':[ResultSetGast[0][0]],    
                'even_strength_goals_gast' : [ResultSetGast[0][1]],
                'even_strength_assists_gast' : [ResultSetGast[0][2]],
                'penalties_in_minutes_gast' : [ResultSetGast[0][3]],
                'power_play_assists_gast' : [ResultSetGast[0][4]], 
                'save_percentage_gast' : [ResultSetGast[0][5]], 
                'saves_gast' : [ResultSetGast[0][6]],
                'shooting_percentage_gast' :[ResultSetGast[0][7]],
                'short_handed_assists_gast' :[ResultSetGast[0][8]], 
                'shots_on_goal_gast' :[ResultSetGast[0][9]], 
                'shutout_gast' : [ResultSetGast[0][10]],
                'duration_gast' : [ResultSetGast[0][11]],
                'corsi_gast' : [ResultSetGast[0][12]],
                'corsi_percentage_gast' : [ResultSetGast[0][13]],
                'faceoff_wins_gast' : [ResultSetGast[0][14]], 
                'faceoff_win_percentage_gast' : [ResultSetGast[0][15]], 
                'fenwick_gast' : [ResultSetGast[0][16]],
                'fenwick_percentage_gast' :[ResultSetGast[0][17]],
                'offensive_zone_start_gast' :[ResultSetGast[0][18]], 
                'power_play_opportunities_gast' :[ResultSetGast[0][19]], 
                'pdo_gast' : [ResultSetGast[0][20]],
                'goals_gast' : [ResultSetGast[0][21]]
            }
            df = pd.DataFrame(d)
            df.to_sql("learning", con=db_connection, if_exists="append", index=False)




def createViewLearningAvg():
    db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
    db_connection = create_engine(db_connection_str)
    #Tabelle learning_avg löschen
    db_connection.execute("Drop table if exists learning_avg")
    
    ResultProxy = db_connection.execute("Select date, home_team, away_team, home_goals, away_goals from schedule where home_goals is not null order by date asc")
    ResultSetSchedule = ResultProxy.fetchall()
    for game in ResultSetSchedule:
        datum = game[0]
        home_goals = game[3]
        away_goals = game[4]
        heim_id = db_connection.execute("Select id from teams where team = '"+str(game[1])+"'").fetchall()[0][0]
        gast_id = db_connection.execute("Select id from teams where team = '"+str(game[2])+"'").fetchall()[0][0]
        ResultSetHeim = db_connection.execute("Select round(avg(assists),0), round(avg(even_strength_goals),0),"+
                                              "round(avg(even_strength_assists),0),"+
                                              "round(avg(penalties_in_minutes),0),"+
                                              "round(avg(power_play_assists),0),"+
                                              "round(avg(save_percentage),0),"+
                                              "round(avg(saves),0),"+
                                              "round(avg(shooting_percentage),0),"+
                                              "round(avg(short_handed_assists),0),"+
                                              "round(avg(shots_on_goal),0),"+
                                              "round(avg(shutout),0),"+
                                              "round(avg(duration),0),"+
                                              "round(avg(corsi),0),"+
                                              "round(avg(corsi_percentage),0),"+
                                              "round(avg(faceoff_wins),0),"+
                                              "round(avg(faceoff_win_percentage),0),"+
                                              "round(avg(fenwick),0),"+
                                              "round(avg(fenwick_percentage),0),"+
                                              "round(avg(offensive_zone_start),0),"+
                                              "round(avg(power_play_opportunities),0),"+
                                              "round(avg(pdo),0),"+
                                              "round(avg(goals),0)"+
                                              " from boxscores where team_id = '"+str(heim_id)+"' and date < '"+str(datum)+"'").fetchall()
        ResultSetGast = db_connection.execute("Select round(avg(assists),0), round(avg(even_strength_goals),0),"+
                                              "round(avg(even_strength_assists),0),"+
                                              "round(avg(penalties_in_minutes),0),"+
                                              "round(avg(power_play_assists),0),"+
                                              "round(avg(save_percentage),0),"+
                                              "round(avg(saves),0),"+
                                              "round(avg(shooting_percentage),0),"+
                                              "round(avg(short_handed_assists),0),"+
                                              "round(avg(shots_on_goal),0),"+
                                              "round(avg(shutout),0),"+
                                              "round(avg(duration),0),"+
                                              "round(avg(corsi),0),"+
                                              "round(avg(corsi_percentage),0),"+
                                              "round(avg(faceoff_wins),0),"+
                                              "round(avg(faceoff_win_percentage),0),"+
                                              "round(avg(fenwick),0),"+
                                              "round(avg(fenwick_percentage),0),"+
                                              "round(avg(offensive_zone_start),0),"+
                                              "round(avg(power_play_opportunities),0),"+
                                              "round(avg(pdo),0),"+
                                              "round(avg(goals),0)"+
                                              "from boxscores where team_id = '"+str(gast_id)+"' and date < '"+str(datum)+"'").fetchall()
        if(ResultSetHeim[0][0] is not None and ResultSetGast[0][0] is not None):
            #ResultSetGast = db_connection.execute("Select avg(assists) from boxscores where team_id = '"+str(gast_id)+"' and date < '"+str(datum)+"'")
            d = {'date':[datum], 'home_goals':[home_goals], 'away_goals':[away_goals],
                 'home_id':[heim_id],'away_id':[gast_id],
                'avg_assists_heim':[ResultSetHeim[0][0]],    
                'avg_even_strength_goals_heim' : [ResultSetHeim[0][1]],
                'avg_even_strength_assists_heim' : [ResultSetHeim[0][2]],
                'avg_penalties_in_minutes_heim' : [ResultSetHeim[0][3]],
                'avg_power_play_assists_heim' : [ResultSetHeim[0][4]], 
                'avg_save_percentage_heim' : [ResultSetHeim[0][5]], 
                'avg_saves_heim' : [ResultSetHeim[0][6]],
                'avg_shooting_percentage_heim' :[ResultSetHeim[0][7]],
                'avg_short_handed_assists_heim' :[ResultSetHeim[0][8]], 
                'avg_shots_on_goal_heim' :[ResultSetHeim[0][9]], 
                'avg_shutout_heim' : [ResultSetHeim[0][10]],
                'avg_duration_heim' : [ResultSetHeim[0][11]],
                'avg_corsi_heim' : [ResultSetHeim[0][12]],
                'avg_corsi_percentage_heim' : [ResultSetHeim[0][13]],
                'avg_faceoff_wins_heim' : [ResultSetHeim[0][14]],
                'avg_faceoff_win_percentage_heim' : [ResultSetHeim[0][15]],
                'avg_fenwick_heim' : [ResultSetHeim[0][16]],
                'avg_fenwick_percentage_heim' : [ResultSetHeim[0][17]],
                'avg_offensive_zone_start_heim' : [ResultSetHeim[0][18]],
                'avg_power_play_opportunities_heim' : [ResultSetHeim[0][19]],
                'avg_pdo_heim' : [ResultSetHeim[0][20]],
                'avg_goals_heim' : [ResultSetHeim[0][21]],
                'avg_assists_gast':[ResultSetGast[0][0]],    
                'avg_even_strength_goals_gast' : [ResultSetGast[0][1]],
                'avg_even_strength_assists_gast' : [ResultSetGast[0][2]],
                'avg_penalties_in_minutes_gast' : [ResultSetGast[0][3]],
                'avg_power_play_assists_gast' : [ResultSetGast[0][4]], 
                'avg_save_percentage_gast' : [ResultSetGast[0][5]], 
                'avg_saves_gast' : [ResultSetGast[0][6]],
                'avg_shooting_percentage_gast' :[ResultSetGast[0][7]],
                'avg_short_handed_assists_gast' :[ResultSetGast[0][8]], 
                'avg_shots_on_goal_gast' :[ResultSetGast[0][9]], 
                'avg_shutout_gast' : [ResultSetGast[0][10]],
                'avg_duration_gast' : [ResultSetGast[0][11]],
                'avg_corsi_gast' : [ResultSetGast[0][12]],
                'avg_corsi_percentage_gast' : [ResultSetGast[0][13]],
                'avg_faceoff_wins_gast' : [ResultSetGast[0][14]], 
                'avg_faceoff_win_percentage_gast' : [ResultSetGast[0][15]], 
                'avg_fenwick_gast' : [ResultSetGast[0][16]],
                'avg_fenwick_percentage_gast' :[ResultSetGast[0][17]],
                'avg_offensive_zone_start_gast' :[ResultSetGast[0][18]], 
                'avg_power_play_opportunities_gast' :[ResultSetGast[0][19]], 
                'avg_pdo_gast' : [ResultSetGast[0][20]],
                'avg_goals_gast' : [ResultSetGast[0][21]]
            }
            df = pd.DataFrame(d)
            df.to_sql("learning_avg", con=db_connection, if_exists="append", index=False)

#Generiert eine Tabelle ähnlich wie ein View, in dem Spiele angezeigt werden, die noch nicht gespielt wurden.
def createViewPredict():
    db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
    db_connection = create_engine(db_connection_str)
    #Vor dem Aufruf sollte die Tabelle to_predict gelöscht werden.
    db_connection.execute("Drop table if exists to_predict")
    #Spiele im Schedule deren Tore Null sind, wurden noch nicht gespielt
    ResultProxy = db_connection.execute("Select date, home_team, away_team from schedule where home_goals is null order by date asc")
    ResultSetSchedule = ResultProxy.fetchall()
    for game in ResultSetSchedule:
        datum = game[0]
        #Team IDs werden geladen für jedes Spiel
        heim_id = db_connection.execute("Select id from teams where team = '"+str(game[1])+"'").fetchall()[0][0]
        gast_id = db_connection.execute("Select id from teams where team = '"+str(game[2])+"'").fetchall()[0][0]
        #Die Durchschnittsdaten werden zusammengerechnet bis vor dem Datum des Spiels.
        ResultSetHeim = db_connection.execute("Select Round(avg(assists),0), round(avg(even_strength_goals),0),"+
                                              "round(avg(even_strength_assists),0),"+
                                              "round(avg(penalties_in_minutes),0),"+
                                              "round(avg(power_play_assists),0),"+
                                              "round(avg(save_percentage),0),"+
                                              "round(avg(saves),0),"+
                                              "round(avg(shooting_percentage),0),"+
                                              "round(avg(short_handed_assists),0),"+
                                              "round(avg(shots_on_goal),0),"+
                                              "round(avg(shutout),0),"+
                                              "round(avg(duration),0),"+
                                              "round(avg(corsi),0),"+
                                              "round(avg(corsi_percentage),0),"+
                                              "round(avg(faceoff_wins),0),"+
                                              "round(avg(faceoff_win_percentage),0),"+
                                              "round(avg(fenwick),0),"+
                                              "round(avg(fenwick_percentage),0),"+
                                              "round(avg(offensive_zone_start),0),"+
                                              "round(avg(power_play_opportunities),0),"+
                                              "round(avg(pdo),0),"+
                                              "round(avg(goals),0) "+
                                              "from boxscores where team_id = '"+str(heim_id)+"' and date < '"+str(datum)+"'").fetchall()
        #Dasselbe für das GastTeam
        ResultSetGast = db_connection.execute("Select round(avg(assists),0), round(avg(even_strength_goals),0),"+
                                              "round(avg(even_strength_assists),0),"+
                                              "round(avg(penalties_in_minutes),0),"+
                                              "round(avg(power_play_assists),0),"+
                                              "round(avg(save_percentage),0),"+
                                              "round(avg(saves),0),"+
                                              "round(avg(shooting_percentage),0),"+
                                              "round(avg(short_handed_assists),0),"+
                                              "round(avg(shots_on_goal),0),"+
                                              "round(avg(shutout),0),"+
                                              "round(avg(duration),0),"+
                                              "round(avg(corsi),0),"+
                                              "round(avg(corsi_percentage),0),"+
                                              "round(avg(faceoff_wins),0),"+
                                              "round(avg(faceoff_win_percentage),0),"+
                                              "round(avg(fenwick),0),"+
                                              "round(avg(fenwick_percentage),0),"+
                                              "round(avg(offensive_zone_start),0),"+
                                              "round(avg(power_play_opportunities),0),"+
                                              "round(avg(pdo),0),"+
                                              "round(avg(goals),0) "+
                                              "from boxscores where team_id = '"+str(gast_id)+"' and date < '"+str(datum)+"'").fetchall()
        #Die Daten werden nach Gast und Heim benannt.
        if(ResultSetHeim[0][0] is not None and ResultSetGast[0][0] is not None):
            d = {'date':[datum], 'home_id':[heim_id],'away_id':[gast_id],
                'avg_assists_heim':[ResultSetHeim[0][0]],    
                'avg_even_strength_goals_heim' : [ResultSetHeim[0][1]],
                'avg_even_strength_assists_heim' : [ResultSetHeim[0][2]],
                'avg_penalties_in_minutes_heim' : [ResultSetHeim[0][3]],
                'avg_power_play_assists_heim' : [ResultSetHeim[0][4]], 
                'avg_save_percentage_heim' : [ResultSetHeim[0][5]], 
                'avg_saves_heim' : [ResultSetHeim[0][6]],
                'avg_shooting_percentage_heim' :[ResultSetHeim[0][7]],
                'avg_short_handed_assists_heim' :[ResultSetHeim[0][8]], 
                'avg_shots_on_goal_heim' :[ResultSetHeim[0][9]], 
                'avg_shutout_heim' : [ResultSetHeim[0][10]],
                'avg_duration_heim' : [ResultSetHeim[0][11]],
                'avg_corsi_heim' : [ResultSetHeim[0][12]],
                'avg_corsi_percentage_heim' : [ResultSetHeim[0][13]],
                'avg_faceoff_wins_heim' : [ResultSetHeim[0][14]],
                'avg_faceoff_win_percentage_heim' : [ResultSetHeim[0][15]],
                'avg_fenwick_heim' : [ResultSetHeim[0][16]],
                'avg_fenwick_percentage_heim' : [ResultSetHeim[0][17]],
                'avg_offensive_zone_start_heim' : [ResultSetHeim[0][18]],
                'avg_power_play_opportunities_heim' : [ResultSetHeim[0][19]],
                'avg_pdo_heim' : [ResultSetHeim[0][20]],
                'avg_goals_heim' : [ResultSetHeim[0][21]],
                'avg_assists_gast':[ResultSetGast[0][0]],    
                'avg_even_strength_goals_gast' : [ResultSetGast[0][1]],
                'avg_even_strength_assists_gast' : [ResultSetGast[0][2]],
                'avg_penalties_in_minutes_gast' : [ResultSetGast[0][3]],
                'avg_power_play_assists_gast' : [ResultSetGast[0][4]], 
                'avg_save_percentage_gast' : [ResultSetGast[0][5]], 
                'avg_saves_gast' : [ResultSetGast[0][6]],
                'avg_shooting_percentage_gast' :[ResultSetGast[0][7]],
                'avg_short_handed_assists_gast' :[ResultSetGast[0][8]], 
                'avg_shots_on_goal_gast' :[ResultSetGast[0][9]], 
                'avg_shutout_gast' : [ResultSetGast[0][10]],
                'avg_duration_gast' : [ResultSetGast[0][11]],
                'avg_corsi_gast' : [ResultSetGast[0][12]],
                'avg_corsi_percentage_gast' : [ResultSetGast[0][13]],
                'avg_faceoff_wins_gast' : [ResultSetGast[0][14]], 
                'avg_faceoff_win_percentage_gast' : [ResultSetGast[0][15]], 
                'avg_fenwick_gast' : [ResultSetGast[0][16]],
                'avg_fenwick_percentage_gast' :[ResultSetGast[0][17]],
                'avg_offensive_zone_start_gast' :[ResultSetGast[0][18]], 
                'avg_power_play_opportunities_gast' :[ResultSetGast[0][19]], 
                'avg_pdo_gast' : [ResultSetGast[0][20]],
                'avg_goals_gast' : [ResultSetGast[0][21]]
            }
            df = pd.DataFrame(d)
            #Zusammengesetzte Daten werden in der to_predict Tabelle abgelegt.
            df.to_sql("to_predict", con=db_connection, if_exists="append", index=False)


            
    #Diese Funktion speichert Daten der sportsreference API in die lokale Datenbank
def importData():
    #Tabellen schedule, teams und boxscores löschen
    db_connection.execute("Drop table if exists schedule, teams, boxscores")
    i = 1
    #Für jedes Team
    for team in Teams():
        #Id wird hochgezählt, Team abkürzung und name werden gespeichert.
        d = {'id':[i],'team':[team.abbreviation],'name':[team.name]}
        df = pd.DataFrame(d)
        df.to_sql("teams", con=db_connection, if_exists="append", index=False)
        for game in team.schedule:
            #Für jedes Spiel im Spielplan des Teams
            #Es wird unterschieden ob es ein Heimspiel ist oder nicht um die richtigen Daten zu speichern
            home = game.location is "Home" 
            if home:
                #Heimspiele werden in der Schedule tabelle abgelegt damit jedes Spiel nur einmal gespeichert wird
                d = {'home_team':[team.abbreviation],'away_team':[game.opponent_abbr], 'home_goals':[game.goals_scored],
                     'away_goals':[game.goals_allowed],'date':[game.datetime],'boxscore_index':[game.boxscore_index]
                }
                df = pd.DataFrame(d)
                df.to_sql("schedule", con=db_connection, if_exists="append", index=False)
            boxscore = game.boxscore
            duration = boxscore.duration
            if(duration is not None):
                s = duration
                duration =  int(s[:-3]) * 60 + int(s[-2:])
            #Boxscore away_skaters muss abgefangen werden, da durch Corona leere Boxscores Objekte zurückgegeben werden
            if hasattr(boxscore, '_away_skaters') and home is True:
                #Alle möglichen Daten aus Boxscore und Game Object werden abgespeichert.
                d = {'team_id':[i],'arena':[boxscore.arena],'attendance':[boxscore.attendance] , 'date':[game.datetime],
                     'duration':[int(duration)], 'corsi': [game.corsi_for], 'corsi_percentage': [game.corsi_for_percentage],
                     'faceoff_wins':[game.faceoff_wins], 'faceoff_win_percentage': [game.faceoff_win_percentage],
                     'fenwick':[game.fenwick_for], 'fenwick_percentage':[game.fenwick_for_percentage],
                     'offensive_zone_start':[game.offensive_zone_start_percentage], 'power_play_opportunities':[game.power_play_opportunities],
                     'pdo':[game.pdo],
                     'assists':[boxscore.home_assists],'even_strength_assists':[boxscore.home_even_strength_assists],
                     'even_strength_goals':[boxscore.home_even_strength_goals], 'game_winning_goals':[boxscore.home_game_winning_goals], 
                     'goals':[boxscore.home_goals],'penalties_in_minutes':[boxscore.home_penalties_in_minutes],
                     'points':[boxscore.home_points],'power_play_assists':[boxscore.home_power_play_assists],
                     'power_play_goals':[boxscore.home_power_play_goals], 'save_percentage':[boxscore.home_save_percentage], 
                     'saves':[boxscore.home_saves],'shooting_percentage':[boxscore.home_shooting_percentage],
                     'short_handed_assists':[boxscore.home_short_handed_assists],'shots_on_goal':[boxscore.home_shots_on_goal],
                     'shutout':[boxscore.home_shutout],'losing_abbr':[boxscore.losing_abbr],
                     'losing_name':[boxscore.losing_name], 'playoff_round':[boxscore.playoff_round], 
                     'time':[boxscore.time],'winner':[boxscore.winner],
                     'winning_abbr':[boxscore.winning_abbr],'winning_name':[boxscore.winning_name]
                }
                df = pd.DataFrame(d)
                df.to_sql("boxscores", con=db_connection, if_exists="append", index=False, dtype={'duration':  Integer})
            if hasattr(boxscore, '_away_skaters') and home is False:
                #Falls das Team gast ist, müssen andere Atrribute gespeichert werden.
                d = {'team_id':[i],'arena':[boxscore.arena],'attendance':[boxscore.attendance] , 'date':[game.datetime],
                     'duration':[int(duration)], 'corsi': [game.corsi_for], 'corsi_percentage': [game.corsi_for_percentage],
                     'faceoff_wins':[game.faceoff_wins], 'faceoff_win_percentage': [game.faceoff_win_percentage],
                     'fenwick':[game.fenwick_for], 'fenwick_percentage':[game.fenwick_for_percentage],
                     'offensive_zone_start':[game.offensive_zone_start_percentage], 'power_play_opportunities':[game.power_play_opportunities],
                     'pdo':[game.pdo],
                     'assists':[boxscore.away_assists] ,'even_strength_assists':boxscore.away_even_strength_assists,
                     'even_strength_goals':[boxscore.away_even_strength_goals],'game_winning_goals' :[boxscore.away_game_winning_goals], 
                     'goals':[boxscore.away_goals],'penalties_in_minutes':[boxscore.away_penalties_in_minutes],
                     'points':[boxscore.away_points], 'power_play_assists':[boxscore.away_power_play_assists], 
                     'power_play_goals':[boxscore.away_power_play_goals], 'save_percentage':[boxscore.away_save_percentage],
                     'saves':[boxscore.away_saves], 'shooting_percentage':[boxscore.away_shooting_percentage],
                     'short_handed_assists':[boxscore.away_short_handed_assists],
                     'short_handed_goals':[boxscore.away_short_handed_goals], 'shots_on_goal':[boxscore.away_shots_on_goal],
                     'shutout':[boxscore.away_shutout], 'losing_abbr':[boxscore.losing_abbr],
                     'losing_name':[boxscore.losing_name], 'playoff_round':[boxscore.playoff_round], 
                     'time':[boxscore.time],'winner':[boxscore.winner],
                     'winning_abbr':[boxscore.winning_abbr],'winning_name':[boxscore.winning_name]
                }
                df = pd.DataFrame(d)
                df.to_sql("boxscores", con=db_connection, if_exists="append", index=False, dtype={'duration':  Integer})
        i = i+1


def get_x_and_y_training():
    db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
    db_connection = create_engine(db_connection_str)
    df_games = pd.DataFrame()
    #Daten werden aus learning Tabelle geladen, in welcher absolute Werte pro Spiel stehen.
    df_games = pd.read_sql('SELECT home_goals, away_goals, assists_heim, assists_gast, even_strength_assists_heim,'+
                           'even_strength_assists_gast, shooting_percentage_heim,'+
                           'shooting_percentage_gast, pdo_heim, pdo_gast, save_percentage_heim,' + 
                           'save_percentage_gast, shots_on_goal_heim, shots_on_goal_gast FROM learning', con=db_connection)
    #Reihen mit leeren Zellen werden gelöscht.
    df_games = df_games.dropna()
    FIELDS_TO_DROP_HOME = ['home_goals', 'away_goals', 'even_strength_assists_gast', 'shooting_percentage_gast', 
                           'save_percentage_heim','assists_gast', 'shots_on_goal_gast']
    #Für das Team überflüssige Daten werden gelöscht. Spalten werden einheitlich benannt.
    X_home = df_games.drop(FIELDS_TO_DROP_HOME, 1)
    X_home = X_home.rename(columns={"assists_heim": "assists", 
                       "even_strength_assists_heim": "even_strength_assists",
                       "shooting_percentage_heim": "shooting_percentage",
                       "pdo_heim": "pdo",
                       "save_percentage_gast": "save_percentage_opp",
                       "pdo_gast" : "pdo_opp",
                       "shots_on_goal_heim": "shots_on_goal"})
    #Das Heimteam erhält das Attribut isHome = 1. Gast 0
    X_home["isHome"] = 1
    y_home = pd.DataFrame()
    y_home['goals'] = df_games["home_goals"]
    FIELDS_TO_DROP_AWAY = ['home_goals', 'away_goals', 'even_strength_assists_heim','shooting_percentage_heim', 
                        'save_percentage_gast','assists_heim', 'shots_on_goal_heim']
    X_away = df_games.drop(FIELDS_TO_DROP_AWAY, 1)
    X_away = X_home.rename(columns={"assists_gast": "assists", 
                       "even_strength_assists_gast": "even_strength_assists",
                       "shooting_percentage_gast": "shooting_percentage",
                       "pdo_gast": "pdo",
                       "save_percentage_heim": "save_percentage_opp",
                       "pdo_heim" : "pdo_opp",
                       "shots_on_goal_gast": "shots_on_goal"})
    X_away["isHome"] = 0
    #Es wird sichergestellt dass die Spalten gleich sortiert sind.
    X_away = X_away[X_home.columns]
    y_away = pd.DataFrame()
    y_away['goals'] = df_games["away_goals"]
    frames = [X_home, X_away]
    #Die Daten werden untereinander geschrieben.
    X = pd.concat(frames, ignore_index=True)
    frames = [y_home, y_away]
    y = pd.concat(frames, ignore_index=True)
    return X, y

def get_x_and_y_testing_avg():
    db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
    db_connection = create_engine(db_connection_str)
    df_games = pd.DataFrame()
    #Daten werden aus der learning_avg Tabelle geladen
    df_games = pd.read_sql('SELECT home_goals, away_goals, avg_shots_on_goal_heim, avg_shots_on_goal_gast, avg_assists_heim, avg_assists_gast, avg_even_strength_assists_heim,'+
                           'avg_even_strength_assists_gast, avg_shooting_percentage_heim,'+
                           'avg_shooting_percentage_gast, avg_pdo_heim, avg_pdo_gast, avg_save_percentage_heim,' + 
                           'avg_save_percentage_gast FROM learning_avg', con=db_connection)
    df_games = df_games.dropna()
    #Für das Heimteam irrelevante Daten werden fallengelassen
    FIELDS_TO_DROP_HOME = ['home_goals', 'away_goals', 'avg_even_strength_assists_gast', 'avg_shooting_percentage_gast', 
                           'avg_save_percentage_heim','avg_assists_gast', 'avg_shots_on_goal_gast']
    X_home = df_games.drop(FIELDS_TO_DROP_HOME, 1)
    #Daten werden einheitlich benannt zum späteren zusammenfügen
    X_home = X_home.rename(columns={"avg_assists_heim": "assists", 
                       "avg_even_strength_assists_heim": "even_strength_assists",
                       "avg_shooting_percentage_heim": "shooting_percentage",
                       "avg_pdo_heim": "pdo",
                       "avg_save_percentage_gast": "save_percentage_opp",
                       "avg_pdo_gast" : "pdo_opp",
                       "avg_shots_on_goal_heim" : "shots_on_goal"})
    X_home["isHome"] = 1
    y_home = pd.DataFrame()
    y_home['goals'] = df_games["home_goals"]
    FIELDS_TO_DROP_AWAY = ['home_goals', 'away_goals', 'avg_even_strength_assists_heim','avg_shooting_percentage_heim', 
                           'avg_save_percentage_gast','avg_assists_heim','avg_shots_on_goal_heim']
    X_away = df_games.drop(FIELDS_TO_DROP_AWAY, 1)
    #Daten werden einheitlich benannt zum späteren zusammenfügen
    X_away = X_home.rename(columns={"avg_assists_gast": "assists", 
                       "avg_even_strength_assists_gast": "even_strength_assists",
                       "avg_shooting_percentage_gast": "shooting_percentage",
                       "avg_pdo_gast": "pdo",
                       "avg_save_percentage_heim": "save_percentage_opp",
                       "avg_pdo_heim" : "pdo_opp",
                       "avg_shots_on_goal_gast" : "shots_on_goal"})
    X_away["isHome"] = 0
    #Es wird sichergestellt, dass die Daten gleich geordnet sind.
    X_away = X_away[X_home.columns]
    y_away = pd.DataFrame()
    y_away['goals'] = df_games["away_goals"]
    frames = [X_home, X_away]
    #Die Daten werden untereinander geschrieben.
    X = pd.concat(frames, ignore_index=True)
    frames = [y_home, y_away]
    y = pd.concat(frames, ignore_index=True)
    return X, y


#Diese Funktion gibt alle Spiele die vorhergesagt werden können am übergebenen Datum zurück.
#Getrennt nach heim und auswärts
def get_x_home_and_away_prediction(date):
    print("Generating predictions for all games on " + date)
    #Trainingsset wird importiert um 
    X, y = get_x_and_y_training()
    db_connection_str = 'mysql+pymysql://root@localhost:3306/nhlprediction'
    db_connection = create_engine(db_connection_str)
    df_games = pd.DataFrame()
    # Nicht vorhergesagte Spiele werden aus der DB geladen. hierbei handelt es sich um durchschnittswerte.
    # Es werden Heim und Gastteam von einander getrennt.
    if date == "ALL":
        print("All")
        mysqlstatement = 'SELECT home_id, away_id, avg_shots_on_goal_heim, avg_shots_on_goal_gast, avg_assists_heim, avg_assists_gast, avg_even_strength_assists_heim, avg_even_strength_assists_gast, avg_shooting_percentage_heim, avg_shooting_percentage_gast, avg_pdo_heim, avg_pdo_gast, avg_save_percentage_heim, avg_save_percentage_gast FROM to_predict'
        df_games = pd.read_sql(mysqlstatement, con=db_connection)
    else:
        mysqlstatement = 'SELECT home_id, away_id, avg_shots_on_goal_heim, avg_shots_on_goal_gast, avg_assists_heim, avg_assists_gast, avg_even_strength_assists_heim, avg_even_strength_assists_gast, avg_shooting_percentage_heim, avg_shooting_percentage_gast, avg_pdo_heim, avg_pdo_gast, avg_save_percentage_heim, avg_save_percentage_gast FROM to_predict where date like %s'
        df_games = pd.read_sql(mysqlstatement, con=db_connection, params=(date+"%",))
    FIELDS_TO_DROP_HOME = ['avg_even_strength_assists_gast', 'avg_shooting_percentage_gast', 
                           'avg_save_percentage_heim','avg_assists_gast', 'avg_shots_on_goal_gast', 'home_id', 'away_id']
    X_home = df_games.drop(FIELDS_TO_DROP_HOME, 1)
    
    #Die Spalten werden einheitlich benannt, damit sie am Ende untereinander geschrieben werden können.
    X_home = X_home.rename(columns={"avg_assists_heim": "assists", 
                       "avg_even_strength_assists_heim": "even_strength_assists",
                       "avg_shooting_percentage_heim": "shooting_percentage",
                       "avg_pdo_heim": "pdo",
                       "avg_save_percentage_gast": "save_percentage_opp",
                       "avg_pdo_gast" : "pdo_opp",
                       "avg_shots_on_goal_heim" : "shots_on_goal"})
    #Als zusätzlicher Faktor ob man zuhause spielt oder nicht
    X_home["isHome"] = 1
    FIELDS_TO_DROP_AWAY = ['avg_even_strength_assists_heim','avg_shooting_percentage_heim', 
                           'avg_save_percentage_gast','avg_assists_heim','avg_shots_on_goal_heim','home_id', 'away_id']
    X_away = df_games.drop(FIELDS_TO_DROP_AWAY, 1)
    #Umbenennung wie oben mit den jeweils entgegengesetzten werten.
    X_away = X_away.rename(columns={"avg_assists_gast": "assists", 
                       "avg_even_strength_assists_gast": "even_strength_assists",
                       "avg_shooting_percentage_gast": "shooting_percentage",
                       "avg_pdo_gast": "pdo",
                       "avg_save_percentage_heim": "save_percentage_opp",
                       "avg_pdo_heim" : "pdo_opp",
                       "avg_shots_on_goal_gast" : "shots_on_goal"})
    #Das Gastteam bekommt für isHome den Wert 0
    X_away["isHome"] = 0
    #Die Spalten werden gleich geordnet wie das Trainingsset und untereinander geschrieben
    X_home = X_home[X.columns]
    X_away = X_away[X_home.columns]
    return df_games, X_home, X_away




def createModelKeras(_epochs):
    createViewLearning()
    createViewLearningAvg()
    X, y = get_x_and_y_training()
    # Testdaten werden aus der DB geladen. Hierbei handelt es sich um die Averagewerte bis vor dem jeweiligen Spiel
    X_test, y_test = get_x_and_y_testing_avg()
    # Die Spalten werden gleich geordnet. Das ist wichtig für die Berechnung
    X_test =  X_test[X.columns]
    print(X.keys())
    def build_model():
        # Neuronales Netzwerk Modell
        model = Sequential([
            # 'Hidden Layer' mit 'input_shape' und relu aktivierung
            Dense(64, activation='relu', input_shape=[len(X.keys())]),
            # 'Hidden Layer' mit relu aktivierung
            Dense(64, activation='relu'),
            # 'Output Layer', linear da für Regression benutzt
            Dense(1)
        ])

        # RMSprop Optimisierungsalgorithmus mit einer 'learningrate' von 0.001
        optimizer = RMSprop(0.001)
        
        # loss='mse' : 'mean squared error' zwischen labels und predictions
        #  metrics=['mae', 'mse']: 'mean absolute error' und 'mean squared error'
        model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
        return model
    
    # Initialisierung des Modells
    model = build_model()
    
    # Initialisierung der 'epochs' über den Funktionsparameter
    EPOCHS = _epochs
    
    # The patience parameter is the amount of epochs to check for improvement
    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=2000)
    early_history = model.fit(X, y, epochs=EPOCHS, validation_split = 0.2, verbose=0, 
                    callbacks=[early_stop, tfdocs.modeling.EpochDots()])
    
    # Zur evaluierung der 'performance' der letzten 'epochs'
    hist = pd.DataFrame(early_history.history)
    hist['epoch'] = early_history.epoch
    #print(hist.tail())
    
    # Evaluierung des Modells an Testdaten
    loss, mae, mse = model.evaluate(X_test, y_test, verbose=2)
    print("Testing set MSE: {:5.2f} goals".format(mse))
    
    # Vorhersage mittels Testdaten
    test_predictions = model.predict(X_test).flatten()
    error = test_predictions - y_test["goals"]
    
    # Ausgabe der Fehlerwerte als plot
    plt.hist(error, bins = 25)
    plt.xlabel("Prediction Error [goals]")
    _ = plt.ylabel("Count")
    
    df = pd.DataFrame()    
    df['pred'] = test_predictions.round(0)
    df['actual'] = y_test['goals']
    df.to_sql("test_prediction", con=db_connection, if_exists="replace", index=False)
    # MeanSquaredError der letzten Berechnung wird ausgegeben
    mse = mean_squared_error(y_test, test_predictions)
    print("MSE" + str(mse))
    # Model wird zur erneuten Verwendung gespeichert.
    pickle.dump(model, open('saved_model_keras.pkl', 'wb'))


def createModelRFR():
    createViewLearning()
    createViewLearningAvg()
    #Trainingsdaten werden aus der DB geladen. X sind die Features, Y die label
    X, y = get_x_and_y_training()
    #Testdaten werden aus der DB geladen. Hierbei handelt es sich um die Averagewerte bis vor dem jeweiligen Spiel
    X_avg, y_avg = get_x_and_y_testing_avg()
    #Die Spalten werden gleich geordnet. Das ist wichtig für die Berechnung
    X_avg = X_avg[X.columns]
    #Aufteilen des Trainingssets in Train und Test um das Model mit nicht-Durchschnittswerten zu validieren
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    #Randomforest Regressor wird initialisiert
    regr = RandomForestRegressor()
    #RFR wird an Daten trainiert.
    regr.fit(X_train, y_train)
    #Genauigkeit des RFR mit den Testsets der Trainingsdaten testen
    y_pred = np.array(regr.predict(X_test).astype(int))
    #MeanSquaredError am Trainigs-Testset wird berechnet und ausgegeben
    mse = mean_squared_error(y_test, y_pred)
    print("MSE_TRAIN: "+ str(mse))
    
    #Model versucht nun Tore anhand der Durchschnittsdaten bis vor dem Spiel zu errechnen.
    y_pred_avg = regr.predict(X_avg).astype(int)
    #MeanSquaredError der letzten Berechnung wird ausgegeben
    mse_avg = mean_squared_error(y_avg, y_pred_avg)
    print("MSE_AVG: "+str(mse_avg))
    df = pd.DataFrame()
    df['pred'] = y_pred_avg.round(0)
    df['actual'] = y_avg['goals']
    #Ergebnisse werden in die Tabelle test_prediction_rfr eingetragen.
    df.to_sql("test_prediction_rfr", con=db_connection, if_exists="replace", index=False)
    #Model wird zur erneuten Verwendung gespeichert.
    pickle.dump(regr, open('saved_model_rfr.pkl', 'wb'))



def loadAndUseKeras(date):
    createViewPredict()
    # Das gespeicherte Model wird geladen
    clf = pickle.load(open('saved_model_keras.pkl', 'rb'))
    # Ansetzungen die noch kein Ergebnis haben werden geladen. X und y abgesplittet
    df, X_home, X_away = get_x_home_and_away_prediction(date)
    for index, row in df.iterrows():
        home_name = db_connection.execute("Select name from teams where id = '"+str(row['home_id'])+"'").fetchall()[0][0]
        away_name = db_connection.execute("Select name from teams where id = '"+str(row['away_id'])+"'").fetchall()[0][0]
        df.loc[(df['home_id']==row['home_id']), 'home_name'] = home_name
        df.loc[(df['away_id']==row['away_id']), 'away_name'] = away_name
       
    if not df.empty:
        # Tore des Heimteams werden berechnet
        y_home = clf.predict(X_home)
        # Tore des Gastteams werden berechnet
        y_away = clf.predict(X_away)

        # Tore werden in das Dataframe zurückgespeichert
        df["predicted_home_goals"] = y_home.round(0)
        df["predicted_away_goals"] = y_away.round(0)
        # Vorhergesagte Spiele werden wieder in die DB geschrieben
        print("Predicted goals for all games on "+ str(date) +":")
        print(df[['home_name', 'away_name', 'predicted_home_goals', 'predicted_away_goals']])
        df.to_sql("predicted_keras", con=db_connection, if_exists="replace", index=False)
    else:
        print("An diesem Tag finden keine Spiele statt")
    
def loadAndUseRFR(date):
    createViewPredict()
    #Das gespeicherte Model wird geladen
    clf = pickle.load(open('saved_model_rfr.pkl', 'rb'))
    #Ansetzungen die noch kein Ergebnis haben werden geladen. X und y abgesplittet
    df, X_home, X_away = get_x_home_and_away_prediction(date)
    for index, row in df.iterrows():
        home_name = db_connection.execute("Select name from teams where id = '"+str(row['home_id'])+"'").fetchall()[0][0]
        away_name = db_connection.execute("Select name from teams where id = '"+str(row['away_id'])+"'").fetchall()[0][0]
        df.loc[(df['home_id']==row['home_id']), 'home_name'] = home_name
        df.loc[(df['away_id']==row['away_id']), 'away_name'] = away_name
    
    if not df.empty:
    
        #Tore des Heimteams werden berechnet
        y_home = clf.predict(X_home)
        #Tore des Gastteams werden berechnet
        y_away = clf.predict(X_away)

        #Tore werden in das Dataframe zurückgespeichert
        df["predicted_home_goals"] = y_home.round(0)
        df["predicted_away_goals"] = y_away.round(0)
        print("Predicted goals for all games on "+ str(date) +":")
        print(df[['home_name', 'away_name', 'predicted_home_goals', 'predicted_away_goals']])
        #Vorhergesagte Spiele werden wieder in die DB geschrieben
        df.to_sql("predicted_rfr", con=db_connection, if_exists="replace", index=False)
    else:
        print("An diesem Tag finden keine Spiele statt")



def main(operation, method, epochs, date):
    if operation == "create":
        if method == "Keras":
            print("Using "+ str(epochs))
            print("Creating Model with Keras")
            createModelKeras(int(epochs))
        elif method == "RFR":
            print("Creating Model with Random Forest Regressor")
            createModelRFR()
        else:
            print("Not a valid Method. RFR or Keras. Case sensitive")
    if operation == "predict":
        if method =="Keras":
            loadAndUseKeras(date)
        elif method =="RFR":
            loadAndUseRFR(date)
        else:
            print("Not a valid Method. RFR or Keras. Case sensitive")
    if operation == "init":
        importData()
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Predict Points')
    parser.add_argument('operation', default="create", help='an integer for the accumulator')
    parser.add_argument('method', default="Keras", help='Specify which model to create. RandomForest or Keras (default: Keras)')
    parser.add_argument('-e', default=4000, help='Specify how many epochs when using keras. (default: 4000)')
    parser.add_argument('-d', default="ALL", help='Which date to predict. Default today.')
    args = parser.parse_args()
    main(args.operation, args.method, args.e, args.d)





