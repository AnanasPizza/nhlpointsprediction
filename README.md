# nhlPointsPrediction

Es soll vorhergesagt werden, wie viele Punkte ein NHL-Team im nächsten Spiel erzielen wird.
Dazu werden Daten der laufenden Saison pro Spiel gesammelt und in Durchschnittswerte umgerechnet.


Datenbereitstellung:

- Mysql Datenbank auf Port 3306
- Datenbank nhlprediction erstellen
- nhlprediction.sql-File in Datenbank dumpen


Benötigte Programme

- Anaconda mit python 3.8

Benötigte python-Module:

- numpy -> pip install numpy
- pandas -> pip install pandas
- sportsreference -> pip install sportsreference
- sklearn -> pip install sklearn
- keras -> pip install keras
- joblib -> pip install joblib
- sqlalchemy -> pip install sqlalchemy
- matplotlib -> pip install matplotlib

Starten der Anwendung:
python nhlprediction.py operation method [-e EPOCHS optional] [-d DATE optional yyyy-mm-dd]

Verschiedene Operationen: 
- create 
- predict
- init

Verschiedene Methoden:
- Keras
- RFR

Epochs:
Wichtig bei create mit Keras. Default wert ist 4000

Date:
An welchem Datum sollen Tore vorhergesagt werden? Default ist ALL, dann werden alle noch nicht gespielten Spiele vorhergesagt.
Schreibweise -d yyyy-mm-dd


Wenn die Datenbank initialisiert wurde, sollte zunächst das gewünschte Model kreeirt werden.
Daraufhin können Spiele, welche noch nicht gespielt wurden vorhergesagt werden.
Die Ergebnisse der Vorhersagen werden in der Datenbank gespeichert in 'predicted\_rfr' und 'predicted\_keras'

Nach den create Methoden wird der Mean Squared Error des Modells auf der Konsole angezeigt.

Vorhergesagte Spiele werden in der DB gespeichert und auf der Konsole ausgegeben.

**Als schneller Test also**
- nhlprediction.sql in eine MySQL Datenbank mit dem Namen nhlprediction oder den Befehl python nhlpredict.py init db ausführen
- Im Projektordner python nhlpredict.py create RFR oder python nhlprediction.py create Keras -e 2000 ausführen. 2000 stehen hier für die Epochs des Keras
- MSE des erstellten Modells an Testdaten wird ausgegeben
- Vergleich der vorhergesagten Daten an den tatsächlichen Daten wird in Tabelle test\_prediction\_rfr oder test\_prediction\_keras abgelegt.
- Anwenden des Modells auf nicht gespielte Spiele mit python nhlprediction.py predict Keras oder python nhlpredict.py predict RFR 
- Vorhergesagte Spiele werden unter predicted\_keras oder predicted\_rfr abgelegt
- Für einen bestimmten Tag, z.B. 12.03.2020: python nhlpredict.py predict RFR -d 2020-03-12


